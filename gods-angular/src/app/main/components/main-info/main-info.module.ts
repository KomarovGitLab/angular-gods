import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { MainInfoRoutingModule } from './main-info-routing.module';
import { MainInfoComponent } from './views/main-info.component';

@NgModule({
    declarations: [
        MainInfoComponent
    ],
    imports: [
        MainInfoRoutingModule,
        SharedModule
    ],
    providers: []
})
export class MainInfoModule { }
