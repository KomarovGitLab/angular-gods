import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BattlesComponent } from './views/battles.component';

const routes: Routes = [
    {
        path: '',
        component: BattlesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BattlesRoutingModule { }