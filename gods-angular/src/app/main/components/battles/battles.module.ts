import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { BattlesRoutingModule } from './battles-routing.module';
import { BattlesComponent } from './views/battles.component';

@NgModule({
    declarations: [
        BattlesComponent,
    ],
    imports: [
        BattlesRoutingModule,
        SharedModule
    ],
    providers: []
})
export class BattlesModule { }
