import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { SoundsRoutingModule } from './sounds-routing.module';
import { SoundsComponent } from './views/sounds.component';

@NgModule({
    declarations: [
        SoundsComponent,
    ],
    imports: [
        SoundsRoutingModule,
        SharedModule
    ],
    providers: []
})
export class SoundsModule { }
