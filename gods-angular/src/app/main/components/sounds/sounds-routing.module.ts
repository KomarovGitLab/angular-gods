import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SoundsComponent } from './views/sounds.component';

const routes: Routes = [
    {
        path: '',
        component: SoundsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SoundsRoutingModule { }