import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GodsComponent } from './views/gods.component';

const routes: Routes = [
    {
        path: '',
        component: GodsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GodsRoutingModule { }