import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { GodsRoutingModule } from './gods-routing.module';
import { GodsComponent } from './views/gods.component';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
    declarations: [
        GodsComponent
    ],
    imports: [
        GodsRoutingModule,
        SharedModule,
        MatTableModule,
        MatSortModule,
    ],
    providers: []
})
export class GodsModule { }
