import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import {MatSort} from '@angular/material/sort';

export interface PeriodicElement {
  name: string;
  family: string;
  info: string;
  element: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {name: 'Один', family: 'Асс', 
  info: 'Верховный бог в германо-скандинавской мифологии, отец и предводитель асов, сын Бора и Бестлы, внук Бури. Мудрец и шаман, знаток рун и сказов (саг), царь-жрец, колдун-воин, бог войны и победы, покровитель военной аристократии, хозяин Вальхаллы и повелитель валькирий.', 
  element: 'Ветер'},
  {name: 'Тор', family: 'Асс', 
  info: 'Бог грома в скандинавской мифологии. Старший сын Одина, вождя норвежских богов. Он владеет могучим молотом Мьельниром. Отец Гуниллы, предводительницы валькирий.', 
  element: 'Молния'},
  {name: 'Локи', family: 'Асс', 
  info: 'Бог хитрости и обмана, сын ётуна Фарбаути и Лаувейи. Единственный Бог, который являлся и ассом и ваном одновременно.', 
  element: 'Вода'},
  {name: 'Фрейя', family: 'Ваны', 
  info: 'Дочь вана, бога моря и ветра Ньёрда, и великанши Скади. Вместе с отцом и братом-близнецом Фреем она жила в Асгарде в качестве заложницы мира после примирения асов с ванами.', 
  element: 'Ветер'},
  {name: 'Фрей', family: 'Ваны', 
  info: 'Бог мира, плодородия, богатства, дождя, лета и солнечного света. Происходит из рода ванов, сын Ньёрда и великанши Скади. Брат-близнец Фрейи и отец Магнуса Чейза, главного героя серии Магнус Чейз и боги Асгарда.', 
  element: 'Огонь'},
];

@Component({
  selector: 'app-gods',
  templateUrl: './gods.component.html',
  styleUrls: ['./gods.component.scss']
})
export class GodsComponent implements OnInit, AfterViewInit {
  
  activeRow: PeriodicElement;
  displayedColumns: string[] = ['name', 'family', 'info', 'element'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  @ViewChild(MatSort) sort: MatSort;

  constructor() {
  }

  ngOnInit(): void {
    
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }
}
