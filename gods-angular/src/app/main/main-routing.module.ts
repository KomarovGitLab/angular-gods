import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './views/main.component';

const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        children: [
            {
                path: 'main-info',
                loadChildren: () => import('./components/main-info/main-info.module').then(module => module.MainInfoModule)
            },
            {
                path: 'gods',
                loadChildren: () => import('./components/gods/gods.module').then(module => module.GodsModule)
            },
            {
                path: 'sounds',
                loadChildren: () => import('./components/sounds/sounds.module').then(module => module.SoundsModule)
            },
            {
                path: 'battles',
                loadChildren: () => import('./components/battles/battles.module').then(module => module.BattlesModule)
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainRoutingModule { }