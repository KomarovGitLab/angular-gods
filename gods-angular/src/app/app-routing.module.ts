import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthorizationComponent } from './authorization/views/authorization.component';

const routes: Routes = [
  {
    path: '',
    component: AuthorizationComponent
  },
  {
    path: 'main',
    loadChildren: () => import('./main/main.module').then(module => module.MainModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
