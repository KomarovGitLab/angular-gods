import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent implements OnInit {
  formAuth: FormGroup;
  formPass: FormGroup;
  hidden = true;
  disabled = true;

  constructor(private router: Router) { }

  ngOnInit(): void {

    this.formAuth = new FormGroup({
      email: new FormControl('', [
        Validators.email, 
        Validators.required
      ]),
      password: new FormControl(null, [
        Validators.required
      ])
    }),

    this.formPass = new FormGroup({
      changePass: new FormControl(null, [
        Validators.required
      ]),
      repeatPass: new FormControl(null, [
        Validators.required
      ])
    })
  }

  goToMain() {
    this.router.navigate(['/main']);
  }

  hideAuth() {
    this.hidden = !this.hidden;
  }

  checkPass() {
    if (this.formPass.get('changePass')?.value === this.formPass.get('repeatPass')?.value) {
      this.hideAuth();
    } else {
      alert('Пароли не совпадают!');
    }
  }

  
}
